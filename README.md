# Mossad.RecruitmentPortal.web

## Getting started
The app is built with .Net Framework 4.7.2 and doesn't really need any specific configurations to start running the app.

Just the clone the app, build and run.

# Screenshots
Following are the sceenshots of the Recruitment Web App

## Home Page
![Alt text](image.png)

## Candidate profile
![Alt text](image-1.png)

## Shortlisted Candidates
![Alt text](image-2.png)

## Rejected Candidates
![Alt text](image-3.png)


