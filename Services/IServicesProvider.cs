﻿using Mossad.RecruitmentPortal.Web.Models;
using System.Collections.Generic;

namespace Mossad.RecruitmentPortal.Web.Services
{
    public interface IServicesProvider
    {
        List<CandidateModel> GetCandidatesMasterList();
        List<TechnologyModel> GetTechnologies();
    }
}