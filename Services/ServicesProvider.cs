﻿using Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper;
using Mossad.RecruitmentPortal.Web.Helpers.Constants;
using Mossad.RecruitmentPortal.Web.Helpers.ServicesConnector;
using Mossad.RecruitmentPortal.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mossad.RecruitmentPortal.Web.Services
{
    public class ServicesProvider : IServicesProvider
    {
        private string EternalBlueAPIBaseUrl { get; set; }
        private IConfigProvider ConfigProvider { get; set; }
        private IExternalServices ExternalServices { get; set; }

        public ServicesProvider(
            IConfigProvider configProvider,
            IExternalServices externalServices)
        {
            ExternalServices = externalServices;
            ConfigProvider = configProvider;
            EternalBlueAPIBaseUrl = configProvider.GetValue<string>(AppSettingsConstants.EternalBlueAPIBaseUrl);
        }

        public ServicesProvider() : this(new ConfigProvider(), new ExternalServices())
        {

        }

        public List<TechnologyModel> GetTechnologies()
        {
            try
            {
                string url = $"{EternalBlueAPIBaseUrl}{AppConstants.EternalBlue.Technologies}";
                string response = ExternalServices.Get(url);
                return JsonConvert.DeserializeObject<List<TechnologyModel>>(response);
            }
            catch (Exception ex)
            {
            }

            return new List<TechnologyModel>();
        }

        public List<CandidateModel> GetCandidatesMasterList()
        {
            try
            {
                string url = $"{EternalBlueAPIBaseUrl}{AppConstants.EternalBlue.Candidates}";
                string response = ExternalServices.Get(url);
                return JsonConvert.DeserializeObject<List<CandidateModel>>(response);
            }
            catch (Exception ex)
            {
            }

            return new List<CandidateModel>();
        }
    }
}