﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Mossad.RecruitmentPortal.Web.Helpers.HttpHandler
{
    public class HttpClientInternal : IHttpClientInternal
    {
        private HttpClient Client { get; set; }

        public HttpClientInternal()
        {
            Client = new HttpClient();
        }

        public async Task<string> GetAsync(string url)
        {
            HttpResponseMessage response = GetHttpResponse(url).Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return string.Empty;
        }

        private async Task<HttpResponseMessage> GetHttpResponse(string url)
        {
            return Client.GetAsync(url).Result;
        }
    }
}