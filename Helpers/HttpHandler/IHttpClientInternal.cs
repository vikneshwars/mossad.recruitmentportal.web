﻿using System.Threading.Tasks;

namespace Mossad.RecruitmentPortal.Web.Helpers.HttpHandler
{
    public interface IHttpClientInternal
    {
        Task<string> GetAsync(string url);
    }
}