﻿namespace Mossad.RecruitmentPortal.Web.Helpers.ServicesConnector
{
    public interface IExternalServices
    {
        string Get(string url);
    }
}