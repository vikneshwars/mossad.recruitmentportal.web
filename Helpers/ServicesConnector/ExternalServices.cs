﻿using Mossad.RecruitmentPortal.Web.Helpers.HttpHandler;
using System;
using System.Net;

namespace Mossad.RecruitmentPortal.Web.Helpers.ServicesConnector
{
    public class ExternalServices : IExternalServices
    {
        public string Get(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return string.Empty;
            }

            try
            {
                return new HttpClientInternal().GetAsync(url).Result;
            }
            catch (WebException webException)
            {
                // log the error
                return string.Empty;
            }
            catch (Exception exception)
            {
                //log the error
                return string.Empty;
            }
        }
    }
}