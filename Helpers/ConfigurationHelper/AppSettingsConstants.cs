﻿namespace Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper
{
    public class AppSettingsConstants
    {
        public const string EternalBlueAPIBaseUrl = "EternalBlueAPIBaseUrl";
        public const string FilteredCandidatesFilePath = "FilteredCandidatesFilePath";
    }
}