﻿using System;
using System.Configuration;

namespace Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper
{
    public class ConfigProvider : IConfigProvider
    {
        public T GetValue<T>(string key)
        {
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key].ToString(), typeof(T));
        }
    }
}