﻿namespace Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper
{
    public interface IConfigProvider
    {
        T GetValue<T>(string key);
    }
}