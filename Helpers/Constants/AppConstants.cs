﻿namespace Mossad.RecruitmentPortal.Web.Helpers.Constants
{
    public class AppConstants
    {
        public class Caching
        {
            public const string CandidateListCacheKey = "CandidateList";
        }

        public class EternalBlue
        {
            public const string Candidates = "/api/candidates";
            public const string Technologies = "/api/technologies";
        }

        public enum Gender
        {
            Male,
            Female,
            Other
        }


    }
}