﻿namespace Mossad.RecruitmentPortal.Web.Models
{
    public class ExperienceModel
    {
        public string TechnologyId { get; set; }
        public string TechnologyName { get; set; }
        public int YearsOfExperience { get; set; }
    }
}