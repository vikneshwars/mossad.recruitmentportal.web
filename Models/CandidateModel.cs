﻿using System.Collections.Generic;
using System.Linq;
using static Mossad.RecruitmentPortal.Web.Helpers.Constants.AppConstants;

namespace Mossad.RecruitmentPortal.Web.Models
{
    public class CandidateModel
    {
        public string CandidateId { get; set; }
        public string FullName { get; set; }
        public int Gender { get; set; }
        public string DisplayGender
        {
            get
            {
                return ((Gender)Gender).ToString();
            }
        }

        public string ProfilePicture { get; set; }
        public string Email { get; set; }
        public List<ExperienceModel> Experience { get; set; }
        public string Skills
        {
            get
            {
                var experience = this.Experience.Select(s => s.TechnologyName);
                return string.Join(" | ", experience);
            }
        }

        public string SkillsToSave { get; set; }
        public int SelectedYearOfExperience { get; set; }
        public string SelectedTechnology { get; set; }

        public string CandidateApprovalStatus { get; set; }
    }
}