﻿namespace Mossad.RecruitmentPortal.Web.Models
{
    public class TechnologyModel
    {
        public string Name { get; set; }
        public string Guid { get; set; }
    }
}