﻿using Mossad.RecruitmentPortal.Web.Models;
using System.Collections.Generic;

namespace Mossad.RecruitmentPortal.Web.Business
{
    public interface IRecruitmentManager
    {
        List<CandidateModel> GetCandidates(string technologyId, int yearsOfExperience);
        List<CandidateModel> GetOrSetCandidatesToCache(string technologyId, int yearsOfExperience);
        List<TechnologyModel> GetTechnologies();
        List<CandidateModel> RejectedCandidates();
        void SaveAsCsvFile(CandidateModel model);
        List<CandidateModel> ShortlistedCandidates();
    }
}