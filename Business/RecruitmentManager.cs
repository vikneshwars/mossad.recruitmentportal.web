﻿using Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper;
using Mossad.RecruitmentPortal.Web.Helpers.Constants;
using Mossad.RecruitmentPortal.Web.Models;
using Mossad.RecruitmentPortal.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Web.Hosting;

namespace Mossad.RecruitmentPortal.Web.Business
{
    public class RecruitmentManager : IRecruitmentManager
    {
        private IServicesProvider ServicesProvider;
        private IConfigProvider ConfigProvider { get; set; }

        public RecruitmentManager(
            IServicesProvider servicesProvider, IConfigProvider configProvider)
        {
            ServicesProvider = servicesProvider;
            ConfigProvider = configProvider;
        }

        public List<TechnologyModel> GetTechnologies()
        {
            return ServicesProvider.GetTechnologies();
        }

        public List<CandidateModel> GetCandidates(string technologyId, int yearsOfExperience)
        {
            var technologies = ServicesProvider.GetTechnologies();

            var filteredCandidates = new List<CandidateModel>();

            // Noticed that the API returns fresh data on each call ie. no duplicate data
            // When the list of candidates for the search criteria is empty, iterate until it matches the search criteria
            while (filteredCandidates.Count == 0)
            {
                var candidatesMasterList = ServicesProvider.GetCandidatesMasterList();

                candidatesMasterList.ForEach(candidate =>
                {
                    foreach (var item in candidate.Experience)
                    {
                        item.TechnologyName = technologies.FirstOrDefault(s => s.Guid == item.TechnologyId).Name;
                    }
                });

                filteredCandidates = GetFilteredCandidates(candidatesMasterList, technologyId, yearsOfExperience);
            }

            return filteredCandidates;
        }

        /// <summary>
        /// Temporarily saving the candidate list in CSV file instead of DB
        /// </summary>
        /// <param name="approvalStatus"></param>
        /// <param name="model"></param>
        public void SaveAsCsvFile(CandidateModel model)
        {
            try
            {
                string separator = ",";
                StringBuilder output = new StringBuilder();

                string[] newLine = {
                        model.CandidateId,
                        model.FullName,
                        model.Email,
                        model.DisplayGender,
                        model.SkillsToSave,
                        model.CandidateApprovalStatus,
                        model.ProfilePicture
                    };

                output.AppendLine(string.Join(separator, newLine));

                var filteredCandidatesFilePath = ConfigProvider.GetValue<string>(AppSettingsConstants.FilteredCandidatesFilePath)
                                                    .Replace("{webroot}", HostingEnvironment.ApplicationPhysicalPath);

                System.IO.File.AppendAllText(filteredCandidatesFilePath, output.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Data could not be written to the CSV file.");
                return;
            }
        }

        public List<CandidateModel> ShortlistedCandidates()
        {
            return ReadFromCsvFile().Where(s => s.CandidateApprovalStatus.Equals("selected")).ToList();
        }

        public List<CandidateModel> RejectedCandidates()
        {
            return ReadFromCsvFile().Where(s => s.CandidateApprovalStatus.Equals("rejected")).ToList();
        }


        /// <summary>
        /// Temporarily using the CSV as the data is not stored in the DB
        /// </summary>
        private List<CandidateModel> ReadFromCsvFile()
        {
            List<CandidateModel> candidateList = new List<CandidateModel>();

            var filteredCandidatesFilePath = ConfigProvider.GetValue<string>(AppSettingsConstants.FilteredCandidatesFilePath)
                                                .Replace("{webroot}", HostingEnvironment.ApplicationPhysicalPath);

            var lines = System.IO.File.ReadAllLines(filteredCandidatesFilePath);

            foreach (var line in lines)
            {
                var parts = line.Split(',');

                CandidateModel candidateModel = new CandidateModel()
                {
                    CandidateId = parts[0],
                    FullName = parts[1],
                    Email = parts[2],
                    SkillsToSave = parts[4],
                    CandidateApprovalStatus = parts[5],
                    ProfilePicture = parts[6]
                };

                candidateList.Add(candidateModel);
            }

            return candidateList;
        }

        public List<CandidateModel> GetOrSetCandidatesToCache(string technologyId, int yearsOfExperience)
        {
            ObjectCache cache = MemoryCache.Default;

            // Store the candidates data in the cache
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();

            if (cache.Contains(AppConstants.Caching.CandidateListCacheKey))
            {
                // get the candidates from the cache
                var cachedCandidateList = (List<CandidateModel>)cache.Get(AppConstants.Caching.CandidateListCacheKey);

                var approvedAndRejectedCandidates = ReadFromCsvFile();

                // never show the selected or rejected candidates
                // this could be done in a better way with linq
                foreach (var approvedAndRejectedCandidate in approvedAndRejectedCandidates)
                {
                    var removeCandidate = cachedCandidateList
                                            .Where(item => item.CandidateId == approvedAndRejectedCandidate.CandidateId)
                                            .FirstOrDefault();

                    cachedCandidateList.Remove(removeCandidate);
                }

                if (cachedCandidateList.Count == 0)
                {
                    var candidates = GetCandidates(technologyId, yearsOfExperience);
                    cache.Add(AppConstants.Caching.CandidateListCacheKey, candidates, cacheItemPolicy);
                    return candidates;
                }

                return cachedCandidateList;
            }
            else
            {
                var candidates = GetCandidates(technologyId, yearsOfExperience);

                // Store the candidates data in the cache
                cache.Add(AppConstants.Caching.CandidateListCacheKey, candidates, cacheItemPolicy);
                return candidates;
            }
        }


        /// <summary>
        /// This method could have been improved using Linq in a better way but didnt have sufficient time
        /// </summary>
        /// <param name="candidates"></param>
        /// <param name="technologyId"></param>
        /// <param name="yearsOfExperience"></param>
        /// <returns></returns>
        private List<CandidateModel> GetFilteredCandidates(
            List<CandidateModel> candidates,
            string technologyId,
            int yearsOfExperience)
        {
            var filteredCandidateList = new List<CandidateModel>();

            foreach (var candidate in candidates)
            {
                foreach (var item in candidate.Experience)
                {
                    if (item.TechnologyId.Equals(technologyId) && item.YearsOfExperience.Equals(yearsOfExperience))
                    {
                        filteredCandidateList.Add(candidate);
                    }
                }
            }

            return filteredCandidateList;
        }
    }
}