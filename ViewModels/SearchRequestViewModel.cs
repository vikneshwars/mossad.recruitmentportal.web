﻿namespace Mossad.RecruitmentPortal.Web.ViewModels
{
    public class SearchRequestViewModel
    {
        public int YearsOfExperience { get; set; }
        public string TechnologyId { get; set; }
    }
}