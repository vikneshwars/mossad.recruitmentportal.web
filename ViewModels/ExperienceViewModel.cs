﻿namespace Mossad.RecruitmentPortal.Web.ViewModels
{
    public class ExperienceViewModel
    {
        public int Id { get; set; }
        public int YearsOfExperience { get; set; }
    }
}