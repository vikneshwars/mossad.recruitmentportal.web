﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Mossad.RecruitmentPortal.Web.ViewModels
{
    public class HomeViewModel
    {
        public int SelectedYearOfExperience { get; set; }
        public string SelectedTechnology { get; set; }
        public IEnumerable<SelectListItem> Experiences { get; set; }
        public IEnumerable<SelectListItem> Technologies { get; set; }
    }
}