﻿using Mossad.RecruitmentPortal.Web.Models;
using System.Collections.Generic;

namespace Mossad.RecruitmentPortal.Web.ViewModels
{
    public class FilteretedCandidatesViewModel
    {
        public List<CandidateModel> ShortlistedCandidates { get; set; }
        public List<CandidateModel> RejectedCandidates { get; set; }
    }

}