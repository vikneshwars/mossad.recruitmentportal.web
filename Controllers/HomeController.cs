﻿using Mossad.RecruitmentPortal.Web.Business;
using Mossad.RecruitmentPortal.Web.Helpers.ConfigurationHelper;
using Mossad.RecruitmentPortal.Web.Models;
using Mossad.RecruitmentPortal.Web.Services;
using Mossad.RecruitmentPortal.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mossad.RecruitmentPortal.Web.Controllers
{
    public class HomeController : Controller
    {
        private IRecruitmentManager RecruitmentManager { get; set; }

        public HomeController()
        {
            RecruitmentManager = new RecruitmentManager(new ServicesProvider(), new ConfigProvider());
        }

        public ActionResult Index()
        {
            HomeViewModel homeViewModel = new HomeViewModel();
            homeViewModel.Experiences = GetExperience();
            homeViewModel.Technologies = GetTechnologies();
            return View(homeViewModel);
        }

        [HttpPost]
        public ActionResult SearchCandidates(HomeViewModel searchRequest)
        {
            return RedirectToAction("FilteredCandidates", new
            {
                technologyId = searchRequest.SelectedTechnology,
                yearsOfExperience = searchRequest.SelectedYearOfExperience
            });
        }

        public ActionResult FilteredCandidates(string technologyId, int yearsOfExperience)
        {
            var candidates = RecruitmentManager.GetOrSetCandidatesToCache(technologyId, yearsOfExperience).FirstOrDefault();
            candidates.SelectedTechnology = technologyId;
            candidates.SelectedYearOfExperience = yearsOfExperience;
            candidates.SkillsToSave = candidates.Skills;
            return View(candidates);
        }

        public ActionResult CandidateSelection(string approvalStatus, CandidateModel model)
        {
            model.CandidateApprovalStatus = approvalStatus;

            // Temporarily saving the candidate list in CSV file instead of DB
            RecruitmentManager.SaveAsCsvFile(model);

            return RedirectToAction("FilteredCandidates", new
            {
                technologyId = model.SelectedTechnology,
                yearsOfExperience = model.SelectedYearOfExperience
            });
        }

        public ActionResult ShortlistedCandidates()
        {
            FilteretedCandidatesViewModel filteretedCandidatesViewModel = new FilteretedCandidatesViewModel();

            filteretedCandidatesViewModel.ShortlistedCandidates = RecruitmentManager.ShortlistedCandidates();

            return View(filteretedCandidatesViewModel);
        }

        public ActionResult RejectedCandidates()
        {
            FilteretedCandidatesViewModel filteretedCandidatesViewModel = new FilteretedCandidatesViewModel();
            filteretedCandidatesViewModel.ShortlistedCandidates = RecruitmentManager.RejectedCandidates();
            return View(filteretedCandidatesViewModel);
        }

        private List<SelectListItem> GetTechnologies()
        {
                var technologies = RecruitmentManager.GetTechnologies();
            return technologies.Select(s => new SelectListItem
                                                    {
                                                        Text = s.Name,
                                                        Value = s.Guid
                                                    }).ToList();
            }

        private List<SelectListItem> GetExperience()
        {
            List<SelectListItem> experienceViewModels = new List<SelectListItem>
            {
                new SelectListItem() { Text = "1", Value = "1", Selected = true },
                new SelectListItem() { Text = "2", Value = "2" },
                new SelectListItem() { Text = "3", Value = "3" },
                new SelectListItem() { Text = "4", Value = "4" },
                new SelectListItem() { Text = "5", Value = "5" },
                new SelectListItem() { Text = "6", Value = "6" },
                new SelectListItem() { Text = "7", Value = "7" },
                new SelectListItem() { Text = "8", Value = "8" },
                new SelectListItem() { Text = "9", Value = "9" },
                new SelectListItem() { Text = "10", Value = "10" }
            };

            return experienceViewModels;
        }
    }
}